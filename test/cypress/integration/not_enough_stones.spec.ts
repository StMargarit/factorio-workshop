/// <reference path="../support/index.d.ts" />
import { Resource } from "../classes/resource";
import { Building } from "../classes/building";
import { PlayerName } from "../classes/playerName";


context('Building', () => {
    beforeEach(() => {
        cy.startGame(PlayerName.Professor);
        // pustit si server, v teardownu stopnout server
    });

    it('tests needed number of stones', function () {
        // pro prvni ctyri nema fungovat
        for (let i = 0; i < 5; i++) {
            if (i != 0) {
                cy.findFirstStone().click();
                cy.mineResource();
                //cy.waitForAmount(Resource.Stone, i, 2); TODO udelat vic stabilni
                cy.tickGame();
            }
            cy.findFirstStone().click();
            cy.checkAlertMessage("#action-BuildMine", "You need 5 stone to build something!");
        }

        // pro patej jo
        cy.findFirstStone().click();
        cy.mineResource();
        cy.tickGame();
        //cy.waitForAmount(Resource.Stone, 5, 2);
        cy.findFirstStone().click();
        cy.buildMine();
        //cy.waitForBuilding(Resource.Stone, Building.Mine, 1);
        cy.tickGame();
        cy.assertBuildingOnTile(Resource.Stone, Building.Mine);
    })
})