/// <reference path="../support/index.d.ts" />

context('Actions', () => {
    beforeEach(() => {
        cy.startGame('Professor');
    });

    it('tests actions', function () {
        cy.findFirstStone().click();
        cy.get('#action-MineResource').should('exist');
        cy.get('#action-BuildMine').should('exist');
        cy.get('#action-BuildIronPlateFactory').should('exist');
        cy.get('#action-BuildSteelFactory').should('exist');
        cy.get('#action-BuildSteelCasingFactory').should('exist');
        cy.get('#action-BuildRocketShellFactory').should('exist');
        cy.get('#action-BuildCopperPlateFactory').should('exist');
        cy.get('#action-BuildWireFactory').should('exist');
        cy.get('#action-BuildCircuitFactory').should('exist');
        cy.get('#action-BuildComputerFactory').should('exist');
        cy.get('#action-BuildPetroleumFactory').should('exist');
        cy.get('#action-BuildBasicFuelFactory').should('exist');
        cy.get('#action-BuildSolidFuelFactory').should('exist');
        cy.get('#action-BuildRocketFuelFactory').should('exist');
    })
})