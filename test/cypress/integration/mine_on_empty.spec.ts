/// <reference path="../support/index.d.ts" />

import { config } from '../../app.config';
import { World } from '../classes/World';
import { Resource } from '../classes/resource';
import { Tile } from '../classes/tile';
import { Building } from '../classes/building';
import { ResourceSite } from '../classes/resourceSite';
import { Coordinates } from '../classes/coordinates';
import { PlayerName } from '../classes/playerName';
import { Action } from '../classes/action';
import * as helper from '../support/helpers';

// na 1.0 pada (bug)
context('mine on empty', () => {
  it('tests that mine cannot be build on empty tile', function () {
    for (let i = 0; i < 5; i++) {
      cy.doActionOnTile(PlayerName.Professor, Action.MineResource, Resource.Stone);
      cy.tickGame();
      let expAmount = i + 1;
      //cy.waitForAmount(Resource.Stone, expAmount, 2);
    }

    cy.doActionOnTile(PlayerName.Professor, Action.BuildMine, Resource.None);
    cy.tickGame(); // nemohu wait, protoze cekam ze se zmena nestane
    cy.assertBuildingOnTile(Resource.None, Building.None);
  });
})