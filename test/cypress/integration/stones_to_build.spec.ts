/// <reference path="../support/index.d.ts" />

import { config } from '../../app.config';
import { World } from '../classes/World';
import { Resource } from '../classes/resource';
import { Tile } from '../classes/tile';
import { Building } from '../classes/building';
import { ResourceSite } from '../classes/resourceSite';
import { Coordinates } from '../classes/coordinates';
import { PlayerName } from '../classes/playerName';
import { Action } from '../classes/action';
import * as helper from '../support/helpers';

const buildingsActions = [
  { Building: Building.BasicFuelFactory, Action: Action.BuildBasicFuelFactory },
  { Building: Building.CircuitFactory, Action: Action.BuildCircuitFactory },
  { Building: Building.ComputerFactory, Action: Action.BuildComputerFactory },
  { Building: Building.CopperPlateFactory, Action: Action.BuildCopperPlateFactory },
  { Building: Building.IronPlateFactory, Action: Action.BuildIronPlateFactory },
  { Building: Building.PetroleumFactory, Action: Action.BuildPetroleumFactory },
  { Building: Building.RocketFuelFactory, Action: Action.BuildRocketFuelFactory },
  { Building: Building.RocketShellFactory, Action: Action.BuildRocketShellFactory },
  { Building: Building.SolidFuelFactory, Action: Action.BuildSolidFuelFactory },
  { Building: Building.SteelCasingFactory, Action: Action.BuildSteelCasingFactory },
  { Building: Building.SteelFactory, Action: Action.BuildSteelFactory },
  { Building: Building.WireFactory, Action: Action.BuildWireFactory },
]

const basicResources = [Resource.IronOre, Resource.Oil, Resource.Stone, Resource.CopperOre]

context('stones to build a building', () => {
  for (let resource of basicResources) {
    it('tests needed number of stones to build ' + Building.Mine + ' on resource ' + resource, function () {
      runTest(resource, Building.Mine, Action.BuildMine);
    });
  }

  for (let buildingAction of buildingsActions) {
    it('tests needed number of stones to build ' + buildingAction.Building, function () {
      runTest(Resource.None, buildingAction.Building, buildingAction.Action);
    });
  }
})

function runTest(resource: Resource, building: Building, action: Action) {
  for (let i = 0; i < 5; i++) {
    if (i != 0) {
      cy.doActionOnTile(PlayerName.Professor, Action.MineResource, Resource.Stone);
    }
    cy.doActionOnTile(PlayerName.Professor, action, resource);
    cy.assertBuildingOnTile(resource, Building.None);
  }

  cy.doActionOnTile(PlayerName.Professor, Action.MineResource, Resource.Stone);
  cy.waitForAmount(Resource.Stone, 5, 10);
  cy.doActionOnTile(PlayerName.Professor, action, resource);
  cy.waitForBuilding(resource, building, 2);
  cy.assertBuildingOnTile(resource, building);
}