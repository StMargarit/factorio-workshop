/// <reference path="../support/index.d.ts" />

import { config } from '../../app.config';
import { World } from '../classes/World';
import { Resource } from '../classes/resource';
import { Tile } from '../classes/tile';
import { Building } from '../classes/building';
import { ResourceSite } from '../classes/resourceSite';
import { Coordinates } from '../classes/coordinates';
import { PlayerName } from '../classes/playerName';
import { Action } from '../classes/action';
import * as helper from '../support/helpers';
import * as constants from '../support/constants';
import { map } from 'cypress/types/bluebird';

context('win', () => {
  beforeEach(() => {
    cy.startGame(PlayerName.Tokyo);
  });

  it('tests creating buildings, winning scenario', function () {
    cy.getWorld().then((response) => {
      let world: World = JSON.parse(response.body);

      helper.checkNumberOfResources(world, Resource.IronOre, constants.neededNumberOfResources);
      helper.checkNumberOfResources(world, Resource.CopperOre, constants.neededNumberOfResources);
      helper.checkNumberOfResources(world, Resource.Oil, constants.neededNumberOfResources);
      helper.checkNumberOfResources(world, Resource.Coal, constants.neededNumberOfCoal);

      let coalCoordinatesArray = helper.findAllResourcesInWorld(world.tiles!, Resource.Coal);
      let stoneCoordinatesArray = helper.findAllResourcesInWorld(world.tiles!, Resource.Stone);
      let ironCoordinatesArray = helper.findAllResourcesInWorld(world.tiles!, Resource.IronOre);
      let copperCoordinatesArray = helper.findAllResourcesInWorld(world.tiles!, Resource.CopperOre);
      let oilCoordinatesArray = helper.findAllResourcesInWorld(world.tiles!, Resource.Oil);
      let emptyCoordinatesArray = helper.findAllResourcesInWorld(world.tiles!, Resource.None);

      let players = new Array<string>();
      for (let player in PlayerName) { 
        players.push(player); 
      }

      cy.mineStonesNeedForStart(players, stoneCoordinatesArray);
      cy.waitForAmountExceeded(Resource.Stone, 63, 10); // 45 needed to build first 9 mines + 18 needed in the next step (27 will be provided by new mines)

      cy.createMines(players, stoneCoordinatesArray)
      cy.createMines(players, coalCoordinatesArray)
      cy.createMines(players, ironCoordinatesArray);
      cy.createMines(players, copperCoordinatesArray);
      cy.createMines(players, oilCoordinatesArray);

      cy.createBuildingsUntilWin(players, emptyCoordinatesArray, 600);
      cy.get('#victory').should('exist');
    });
  });
})