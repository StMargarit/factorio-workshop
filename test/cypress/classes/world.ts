import { Tile } from "./tile";

export class World {
  tiles?: Array<Array<Tile>>;

  constructor(tiles: Array<Array<Tile>>) {
    this.tiles = tiles
  }
}