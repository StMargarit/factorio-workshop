export enum Resource {
    None = "None",
    Stone = "Stone",
    Oil = "Oil",
    Coal = "Coal",
    CopperOre = "CopperOre",
    IronOre = "IronOre",
    Computer = "Computer",
    SteelCasing = "SteelCasing",
    RocketShell = "RocketShell",
    CopperPlate = "CopperPlate",
    Circuit = "Circuit",
    SolidFuel = "SolidFuel",
    IronPlate = "IronPlate",
    RocketFuel = "RocketFuel",
    Wire = "Wire",
    Petroleum = "Petroleum",
    BasicFuel = "BasicFuel",
    Steel = "Steel"
}