export class Coordinates {
    X: number;
    Y: number;

    constructor(X: number, Y: number) {
        this.X = X;
        this.Y = Y;
    }
}