export enum Building {
    None = "None",
    Mine = "Mine", // for all same
    IronPlateFactory = "IronPlateFactory",
    SteelFactory = "SteelFactory",
    SteelCasingFactory = "SteelCasingFactory",
    RocketShellFactory = "RocketShellFactory",
    CopperPlateFactory = "CopperPlateFactory",
    WireFactory = "WireFactory",
    CircuitFactory = "CircuitFactory",
    ComputerFactory = "ComputerFactory",
    PetroleumFactory = "PetroleumFactory",
    BasicFuelFactory = "BasicFuelFactory",
    SolidFuelFactory = "SolidFuelFactory",
    RocketFuelFactory = "RocketFuelFactory"
}