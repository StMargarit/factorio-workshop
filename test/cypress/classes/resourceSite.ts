import { Resource } from "./resource";

export class ResourceSite {
  Amount?: number;
  Resource?: Resource;

  constructor(Amount: number, Resource: Resource) {
    this.Amount = Amount;
    this.Resource = Resource;
  }
}