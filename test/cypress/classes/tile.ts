import { Building } from "./building";
import { ResourceSite } from "./resourceSite";

export class Tile {
  building?: Building;
  resourceSite?: ResourceSite;

  constructor(building: Building, resourceSite: ResourceSite) {
    this.building = building;
    this.resourceSite = resourceSite;
  }
}