export enum PlayerName {
    Nairobi = 'Nairobi',
    Tokyo = 'Tokyo',
    Berlin = 'Berlin',
    Denver = 'Denver',
    Helsinki = 'Helsinki',
    Rio = 'Rio',
    Moscow = 'Moscow',
    Oslo = 'Oslo',
    Professor = 'Professor'
}