/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable<Subject> {
        startGame(playerName: string): Chainable<Subject>
        tickGame(): Chainable<Subject>
        findFirstStone(): Chainable<Subject>
        mineResource(): Chainable<Subject>
        buildMine(): Chainable<Subject>
        checkAlertMessage(action: any, message: any): Chainable<Subject>
        getWorld(): Chainable<Cypress.Response>
        getBank(): Chainable<Cypress.Response>
        postTurns(playerName: any, action: any, coordinates: any): Chainable<Cypress.Response> // TODO normalni typy nejdou proc?!
        restartServer(): Chainable<Cypress.Response>
        doActionOnTile(playerName: any, action: any, resource: any): Chainable<Subject>
        assertBuildingOnTile(resource: any, building: any): Chainable<Subject>
        waitForAmount(resource: any, expectedAmount: any, timeoutSeconds: any): Chainable<Subject>
        waitForAmountExceeded(resource: any, expectedAmount: any, timeoutSeconds: any): Chainable<Subject>
        waitForBuilding(resource: any, expectedBuilding: any, timeoutSeconds: any): Chainable<Subject>
        waitForBuildingOnCoordinates(coordinates: any, expectedBuilding: any, timeoutSeconds: any): Chainable<Subject>
        createBuildingsUntilWin(players: any, emptyCoordinatesArray: any, timeoutSeconds: any): Chainable<Subject>
        createMines(players: any, coordinatesArray: any): Chainable<Subject>
        mineStonesNeedForStart(players: any, coordinatesArray: any): Chainable<Subject>
    }
}