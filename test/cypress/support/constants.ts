const neededNumberOfPreviousResource = 3;
export const neededNumberOfFinalResources = 100;
export const neededNumberOfResources = (neededNumberOfPreviousResource ** 4) * neededNumberOfFinalResources;
export const neededNumberOfCoal = neededNumberOfFinalResources + (neededNumberOfFinalResources * neededNumberOfPreviousResource) +
    (neededNumberOfFinalResources * neededNumberOfPreviousResource ** 2) + (neededNumberOfFinalResources * neededNumberOfPreviousResource ** 3);