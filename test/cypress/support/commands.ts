import { World } from "../classes/world";
import * as helper from './helpers';
import * as constants from './constants';
import { PlayerName } from "../classes/playerName";
import { Action } from "../classes/action";
import { Resource } from "../classes/resource";
import { Building } from "../classes/building";
import { ResourceSite } from "../classes/resourceSite";

Cypress.Commands.add('startGame', (playerName) => {
    cy.visit('/');
    cy.get('select').select(playerName);
    cy.get('button').click(); // play as default player
});

Cypress.Commands.add('tickGame', () => {
    cy.wait(1000); // game ticks like that
});

Cypress.Commands.add('stoneDeposit', () => {
    return cy.get('#bank-Stone-amount').invoke('text');
});

Cypress.Commands.add('findFirstStone', () => {
    return cy.get('td > div > img[alt="Icon of Stone"]:eq(0)');
});

Cypress.Commands.add('findNthStone', (number) => {
    return cy.get('td > div > img[alt="Icon of Stone"]:eq(' + number + ')');
});

Cypress.Commands.add('mineResource', () => {
    cy.get('#action-MineResource').click();
});

Cypress.Commands.add('buildMine', () => {
    cy.get('#action-BuildMine').click();
});

Cypress.Commands.add('checkAlertMessage', (action, message) => {
    const stub = cy.stub();
    cy.on('window:alert', stub);
    cy
        .get(action).click()
        .then(() => {
            expect(stub.getCall(0)).to.be.calledWith(message)
        });
});

Cypress.Commands.add('doActionOnTile', (playerName, action, resource) => {
    cy.getWorld().then((response) => {
        let world: World = JSON.parse(response.body);
        let coordinates = helper.getCoordinatesOfResource(world, response, resource);
        cy.postTurns(playerName, action, coordinates);
    });
});

Cypress.Commands.add('assertBuildingOnTile', (resource, building) => {
    cy.getWorld().then((response) => {
        let world: World = JSON.parse(response.body);
        let coordinates = helper.getCoordinatesOfResource(world, response, resource);
        assert.equal(world.tiles![coordinates!.Y][coordinates!.X].building, building); // TODO undefined? 
    });
});

Cypress.Commands.add('waitForAmount', (resource, expectedAmount, timeoutSeconds) => {
    let start = new Date();
    function waitForAmountRetries(): any {
        return cy.getBank()
            .then(response => {
                let bank: Array<ResourceSite> = JSON.parse(response.body);
                let amount = helper.getAmountInBank(bank, resource);
                if (amount === expectedAmount) {
                    return response;
                } else {
                    if ((new Date().valueOf() - start.valueOf()) >= timeoutSeconds * 1000) throw new Error('Wait for Amount timeouted.')
                    cy.wait(50);
                    return waitForAmountRetries();
                }
            });
    }
    return waitForAmountRetries();
});

Cypress.Commands.add('waitForAmountExceeded', (resource, expectedAmount, timeoutSeconds) => {
    let start = new Date();

    function waitForAmountExceeded(): any {
        return cy.getBank()
            .then(response => {
                let bank: Array<ResourceSite> = JSON.parse(response.body);
                let amount = helper.getAmountInBank(bank, resource);
                if (amount! >= expectedAmount) {
                    return response;
                } else {
                    if ((new Date().valueOf() - start.valueOf()) >= timeoutSeconds * 1000) throw new Error('Wait for Amount Exceeded timeouted.')
                    cy.wait(50);
                    return waitForAmountExceeded();
                }
            });
    }
    return waitForAmountExceeded();
});

Cypress.Commands.add('waitForBuilding', (resource, expectedBuilding, timeoutSeconds) => {
    let start = new Date();

    function waitForBuildingRetries(): any {
        return cy.getWorld()
            .then((response) => {
                let world: World = JSON.parse(response.body);
                let coordinates = helper.getCoordinatesOfResource(world, response, resource);
                if (world.tiles![coordinates!.Y][coordinates!.X].building === expectedBuilding) {
                    return response;
                } else {
                    if ((new Date().valueOf() - start.valueOf()) >= timeoutSeconds * 1000) throw new Error('Wait for Building timeouted.')
                    cy.wait(50);
                    return waitForBuildingRetries();
                }
            });
    }
    return waitForBuildingRetries();
});

Cypress.Commands.add('waitForBuildingOnCoordinates', (coordinates, expectedBuilding, timeoutSeconds) => {
    let start = new Date();

    function waitForBuildingOnCoordinates(): any {
        return cy.getWorld()
            .then((response) => {
                let world: World = JSON.parse(response.body);
                if (world.tiles![coordinates!.Y][coordinates!.X].building === expectedBuilding) {
                    return response;
                } else {
                    if ((new Date().valueOf() - start.valueOf()) >= timeoutSeconds * 1000) throw new Error('Wait for Building on coordinates timeouted.')
                    cy.wait(50);
                    return waitForBuildingOnCoordinates();
                }
            });
    }
    return waitForBuildingOnCoordinates();
});

Cypress.Commands.add('createBuildingsUntilWin', (players, emptyCoordinatesArray, timeoutSeconds) => {
    let mineIndex = 0;
    let coalMineIndex = 0;
    let emptyIndex = 0;
    let start = new Date();

    function createBuildings(): any { 
        cy.getBank()
            .then(response => {
                let bank: Array<ResourceSite> = JSON.parse(response.body);
                let amountRocketShell = helper.getAmountInBank(bank, Resource.RocketShell);
                let amountComputer = helper.getAmountInBank(bank, Resource.Computer);
                let amountRocketFuel = helper.getAmountInBank(bank, Resource.RocketFuel);

                if (amountRocketShell! >= constants.neededNumberOfFinalResources
                    && amountComputer! >= constants.neededNumberOfFinalResources
                    && amountRocketFuel! >= constants.neededNumberOfFinalResources) {
                    return response;
                } else {
                    if ((new Date().valueOf() - start.valueOf()) >= timeoutSeconds * 1000) throw new Error('Creating building and wait for win timeouted.')

                    let actions = new Array<Action>();
                    actions = [
                        Action.BuildIronPlateFactory, Action.BuildCopperPlateFactory, Action.BuildPetroleumFactory,
                        Action.BuildIronPlateFactory, Action.BuildCopperPlateFactory, Action.BuildPetroleumFactory,
                        Action.BuildIronPlateFactory, Action.BuildCopperPlateFactory, Action.BuildPetroleumFactory,
                        Action.BuildSteelFactory, Action.BuildWireFactory, Action.BuildBasicFuelFactory,
                        Action.BuildSteelFactory, Action.BuildWireFactory, Action.BuildBasicFuelFactory,
                        Action.BuildSteelCasingFactory, Action.BuildCircuitFactory, Action.BuildSolidFuelFactory,
                        Action.BuildRocketShellFactory, Action.BuildComputerFactory, Action.BuildRocketFuelFactory
                    ];
                    let emptyCoords = emptyCoordinatesArray;

                    for (let i = 0; i < actions.length; i++) {
                        let toBeBuilt = emptyCoords.shift();
                        cy.postTurns(players[i % 9], actions[i], toBeBuilt);
                    }
                    return createBuildings();
                }
            });
    }
    return createBuildings();
});

Cypress.Commands.add('mineStonesNeedForStart', (players, coordinatesArray) => {
    for (let i = 0; i < 7; i++) {
        for (let j in players) {
            if (coordinatesArray[j]) {
                cy.postTurns(players[j], Action.MineResource, coordinatesArray[j]);
            }
        }
    }
});

Cypress.Commands.add('createMines', (players, coordinatesArray) => {
    for (let i = 0; i < coordinatesArray.length; i++) {
        cy.postTurns(players[i % 9], Action.BuildMine, coordinatesArray[i]);
    }
});