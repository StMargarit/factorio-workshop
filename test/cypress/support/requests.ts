import { config } from '../../app.config';

Cypress.Commands.add('getWorld', () => {
    cy.request({
        method: 'GET',
        url: config.serverUrl + '/world'
    });
});

Cypress.Commands.add('getBank', () => {
    cy.request({
        method: 'GET',
        url: config.serverUrl + '/bank'
    });
});

Cypress.Commands.add('postTurns', (playerName, action, coordinates) => {
    cy.request({
        method: 'POST',
        url: config.serverUrl + '/turns',
        body: {
            PlayerName: playerName,
            action: action,
            coordinates: coordinates
        }
    });
});

Cypress.Commands.add('restartServer', (playerName, action, coordinates) => {
    cy.request({
        method: 'POST',
        url: config.serverUrl + '/restart'
    });
});

