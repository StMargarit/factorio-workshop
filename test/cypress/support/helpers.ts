import { Tile } from "../classes/tile";
import { Coordinates } from "../classes/coordinates";
import { Resource } from "../classes/resource";
import { World } from "../classes/world";
import { ResourceSite } from "../classes/resourceSite";
import { Building } from "../classes/building";
import * as constants from './constants';

export function findFirstResourceInWorld(array: Array<Array<Tile>>, resource: string): Coordinates | undefined {
    for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < array[i].length; j++) {
            if (array[i][j].resourceSite!.Resource === resource) {
                return new Coordinates(j, i);
            }
        }
    }
    return undefined;
}

export function findAllResourcesInWorld(array: Array<Array<Tile>>, resource: string): Array<Coordinates> {
    let coordinatesArrray = new Array<Coordinates>();
    for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < array[i].length; j++) {
            if (array[i][j].resourceSite!.Resource === resource) {
                coordinatesArrray.push(new Coordinates(j, i));
            }
        }
    }
    return coordinatesArrray;
}

export function getCoordinatesOfResource(world: World, response: Cypress.Response, resource: Resource): Coordinates | undefined {
    debugger
    if (world.tiles) {
        let tileWithStonePosition = findFirstResourceInWorld(world.tiles, resource);
        if (tileWithStonePosition) {
            return tileWithStonePosition;
        }
    }
    return undefined;
}

export function getAmountInBank(array: Array<ResourceSite>, resource: Resource): number | undefined {
    for (var i = 0; i < array.length; i++) {
        if (array[i].Resource === resource) {
            return array[i].Amount;
        }
    }
    return undefined;
}

export function checkNumberOfResources(world: World, resource: Resource, expNumber: number) {
    let numberOfResourcesOnMap = 0;
    for (var i = 0; i < world.tiles!.length; i++) {
        for (var j = 0; j < world.tiles![i].length; j++) {
            if (world.tiles![i][j].resourceSite!.Resource === resource) {
                numberOfResourcesOnMap += world.tiles![i][j].resourceSite!.Amount!;
            }
        }
    }

    assert.isAbove(numberOfResourcesOnMap, expNumber, "Number of resources: " + resource + " to win");
}