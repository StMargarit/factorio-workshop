export interface ApplicationConfig {
  serverUrl: string;
}

export const config: ApplicationConfig = {
  serverUrl: 'http://localhost:8080'
};